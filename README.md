# README #

1/3スケールのぴゅう太風小物のstlファイルです。
スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。
元ファイルはAUTODESK 123D DESIGNです。


***

# 実機情報

## メーカ
- トミー

## 発売時期
- 1982年8月20日

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/%E3%81%B4%E3%82%85%E3%81%86%E5%A4%AA)
- [「僧兵ちまちま」のゲーム日記。](https://blog.goo.ne.jp/timatima-psu/e/4ee3df3adab38d5c47bd54c576e772b1)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_pyuuta/raw/b2a12424b071f9bed0863c5c8b45e11e7be62831/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_pyuuta/raw/b2a12424b071f9bed0863c5c8b45e11e7be62831/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_pyuuta/raw/b2a12424b071f9bed0863c5c8b45e11e7be62831/ExampleImage.jpg)
![](https://bitbucket.org/kyoto-densouan/3dmodel_pyuuta/raw/b2a12424b071f9bed0863c5c8b45e11e7be62831/ExampleImage_2.jpg)
